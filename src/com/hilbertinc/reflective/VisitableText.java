package com.hilbertinc.reflective;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 12/5/13
 * Time: 5:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class VisitableText implements Visitable {

    private String _text;

    public VisitableText(String text) {
        this._text = text;
    }

    @Override
    public void accept(Visitor v) {
        v.visit(this);
    }

    @Override
    public String html() {
        String text = "<h4>Text</h4>\n";
        text += "\t<p>\n";
        text += "\t\t" + this._text + "\n";
        text += "\t</p>";
        return text;
    }
}
