package com.hilbertinc.reflective;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 12/5/13
 * Time: 5:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class VisitableElement implements Visitable {

    private String _nodeName;

    public VisitableElement(String nodeName) {
        this._nodeName = nodeName;
    }

    @Override
    public void accept(Visitor v) {
        v.visit(this);
    }

    @Override
    public String html() {
        return "<h2>" + this._nodeName + "</h2>";
    }
}
