package com.hilbertinc.reflective;

public interface Visitable
{
	void accept(Visitor v);

    public String html();
}
