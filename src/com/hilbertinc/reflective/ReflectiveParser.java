package com.hilbertinc.reflective;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 12/5/13
 * Time: 5:48 AM
 * To change this template use File | Settings | File Templates.
 */
public class ReflectiveParser {

    private static List<Visitable> visitables = new ArrayList<Visitable>();

    public static List<Visitable> Parse(Document document) {
        // do the xml stuff here
        _parse(document);
        return visitables;
    }

    private static void _parse(Node node) {
        if (node.hasChildNodes()) {
            if (node.getNodeName().compareToIgnoreCase("#document") > 0) {
                if (node.getNodeName().compareToIgnoreCase("text") == 0) {
                    visitables.add(new VisitableText(node.getTextContent()));
                } else {
                    visitables.add(new VisitableElement(node.getNodeName()));
                }
            }
        }
        if (node.hasChildNodes()) {
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                _parse(node.getChildNodes().item(i));
            }
        }
    }

}
