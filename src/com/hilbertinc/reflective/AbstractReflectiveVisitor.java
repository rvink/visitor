package com.hilbertinc.reflective;

import java.lang.reflect.*;
import java.util.*;


public class AbstractReflectiveVisitor implements Visitor {

	// ClassMap is a static cache hashmap of hashmaps that 
	// stores previously located visit methods on visitor 
	// classes.  The visitor class is the outer key of this 
	// cache.  The element-visited class is the inner key. 
	// The values of the inner hashmaps are the visit methods 
	// located on the respective visitor classes.
	protected static Hashtable ClassMap = new Hashtable();

	// VISITOR METHODS
  
	public void dispatch( Object object ) {
		try {
			// find the method to invoke
			Method method = getMethod( object.getClass() );

			// invoke the visitXXX method where XXX is the visitedelementclassname
			Object result = method.invoke( this, new Object[] { object } );

			// lastly, invoke callAccept ONLY if the element is of Visitable type
			if( !Boolean.FALSE.equals( result ) && object instanceof Visitable )
				callAccept( (com.hilbertinc.reflective.Visitable) object );
		}
		catch( InvocationTargetException ex ) {
			ex.getTargetException().printStackTrace();
			throw new RuntimeException(ex.toString());
		}
		catch( Exception e ) {
			throw new RuntimeException(e.toString());
		}
	}

	// VISIT METHODS
  
	public void visit( Object object ) {
	}

	// SUPPORT METHODS
  
	public void callAccept( com.hilbertinc.reflective.Visitable v ) throws Exception {
		v.accept( this );
	}

    // 1. Look for visitElementClassName() in the current class
    // 2. Look for visitElementClassName() in superclasses
    // 3. Look for visitElementClassName() in interfaces
    // 4. Look for visitObject() in current class
    protected Method getMethod( Class c ) {
        Class  newc = c;
        Method m    = null;
        while (m == null  &&  newc != Object.class) {
            String method = newc.getName();
            method = "visit" + method.substring( method.lastIndexOf('.') + 1 );
            try {
                m = getClass().getMethod( method, new Class[] { newc } );
            } catch (NoSuchMethodException ex) {
                newc = newc.getSuperclass();
            }
        }
        if (newc == Object.class) {
            // System.out.println( "Searching for interfaces" );
            Class[] interfaces = c.getInterfaces();
            for (int i=0; i < interfaces.length; i++) {
                String method = interfaces[i].getName();
                method = "visit" + method.substring( method.lastIndexOf('.') + 1 );
                try {
                    m = getClass().getMethod( method, new Class[] { interfaces[i] } );
                } catch (NoSuchMethodException ex) { }
            }
        }
        if (m == null)
            try {
                m = getClass().getMethod( "visitObject", new Class[] { Object.class } );
            } catch (Exception ex) { }
        return m;
    }
}
