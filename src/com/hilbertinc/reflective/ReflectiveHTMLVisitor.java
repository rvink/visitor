package com.hilbertinc.reflective;

import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 12/4/13
 * Time: 8:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReflectiveHTMLVisitor extends AbstractReflectiveVisitor {

    private int indent = 0;

    private PrintWriter printWriter;



    public ReflectiveHTMLVisitor(String fileName) {
        try {
            this.printWriter = new PrintWriter(fileName);
        } catch (Exception e) {
            System.out.println("couldn't create the html file");
        }
    }

    public void start() {
        printWriter.println("<html>");
        printWriter.println("<body>");
        this.indent += 1;

    }

    public void stop() {
        this.indent -= 1;
        printWriter.println("</body>");
        printWriter.println("</html>");
        printWriter.close();
    }

    @Override
    public void visit(Object object) {
        try {
            getMethod(object.getClass()).invoke(this, new Object[]{object});
        } catch (Exception e) {
            System.out.println("no appropriate visit method");
        }
    }

    public void visitVisitableText(VisitableText visitableText) {
        this.printWriter.println(this.indent() + visitableText.html());
        System.out.println(this.indent() + visitableText.html());
    }

    public void visitVisitableElement(VisitableElement visitableElement) {
        this.printWriter.println(this.indent() + visitableElement.html());
        System.out.println(visitableElement.html());
    }

    private String indent() {
        String indentation = "";
        for(int i = 0; i < this.indent; i++) {
            indentation += "\t";
        }
        return indentation;
    }

}
