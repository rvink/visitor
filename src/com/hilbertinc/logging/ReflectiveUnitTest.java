package com.hilbertinc.logging;

import com.hilbertinc.reflective.*;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 12/4/13
 * Time: 8:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReflectiveUnitTest {

    public ReflectiveUnitTest() {
        super();
    }

    public static void main(String[] args) {
        try {
            File fXmlFile = new File("infolog.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(fXmlFile);
            document.getDocumentElement().normalize();
            List<Visitable> visitables = ReflectiveParser.Parse(document);


            ReflectiveHTMLVisitor reflectiveHTMLVisitor = new ReflectiveHTMLVisitor("infolog.html");

            reflectiveHTMLVisitor.start();
            for(Visitable visitable : visitables) {
                visitable.accept(reflectiveHTMLVisitor);
            }
            reflectiveHTMLVisitor.stop();

            fXmlFile = new File("samplelog.xml");
            document = documentBuilder.parse(fXmlFile);
            document.getDocumentElement().normalize();
            reflectiveHTMLVisitor = new ReflectiveHTMLVisitor("samplelog.html");

            visitables = ReflectiveParser.Parse(document);

            reflectiveHTMLVisitor.start();
            for(Visitable visitable : visitables) {
                visitable.accept(reflectiveHTMLVisitor);
            }
            reflectiveHTMLVisitor.stop();



        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
