package com.assignment;

public interface Visitor
{
	void visit(Object object);
}
