package com.assignment;

import com.hilbertinc.base.HException;
import com.hilbertinc.xml.HDOMVisitor;
import com.ibm.xml.parser.Parser;
import org.w3c.dom.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 11/29/13
 * Time: 4:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class DOMParser {

    private Document document;

    /**
     * HXmlParser constructor comment.
     */
    public DOMParser() {
        super();
    }

    /**
     * Access the DOM document that is the result of the parsed XML
     * document.
     *
     * @return org.w3c.dom.Document
     */
    public Document getDocument() {
        return document;
    }

    /**
     * This will parse the XML data from the input stream passed
     *
     * @param xmlstream InputStream
     */
    public void parse(InputStream xmlstream)
            throws HException {
        Parser parser = new Parser("XML from stream");
        setDocument(parser.readStream(xmlstream));
        return;
    }

    /**
     * This will parse the XML data from the input stream passed
     *
     * @param xmlstream InputStream
     */
    public void parse(InputStream xmlstream, String name)
            throws HException {
        Parser parser = new Parser(name);
        setDocument(parser.readStream(xmlstream));
        return;
    }

    /**
     * This is a convenience method that will build a FileInputStream from the
     * file name passed and call the parser on the stream
     *
     * @param fileName java.lang.String
     */
    public void parse(String fileName)
            throws HException {
        try {
            FileInputStream stream = new FileInputStream(fileName);
            parse(stream, fileName);
        } catch (IOException exception) {
            HException hException = new HException(exception, "I/O error parsing file: " + fileName);
            hException.setClassName("HDOMParser");
            hException.setMethod("parse(String)");
            throw hException;
        }
        return;
    }

    /**
     * This does the actual work of traversing the DOM tree.  This is called
     * by the public traverse(...) method with the root as the node.  We
     * terminate the traversal as soon as the visitor returns a 'false' or
     * the entire tree has been traversed
     *
     * @param visitor COM.hilbertinc.xml.HDOMVisitor
     * @param node    org.w3c.dom.Element
     */
    protected void processNode(DOMVisitor visitor, Node node)
            throws Exception {

        // --- Document nodes ---

        return;
    }

    /**
     * Access the DOM document that is the result of the parsed XML
     * document.
     *
     * @param dom org.w3c.dom.Document
     */
    public void setDocument(Document dom) {
        document = dom;
        return;
    }

    /**
     * This will traverse the DOM tree and perform actions on a node within
     * the tree
     *
     * @param visitor HDOMVisitor
     */
    public void traverse(DOMVisitor visitor)
            throws Exception {
        if (null == getDocument())
            return; // Nothing to traverse

        if (null == visitor)
            return; // Nothing to do once we get to a node, so we might as well leave now

        NodeList children = getDocument().getChildNodes();
        int count = children.getLength();
        for (int i = 0; i < count; ++i) {
            Node child = (Node) children.item(i);
            processNode(visitor, child);
        }
        return;
    }
}
