package com.assignment;

public interface Visitable
{
	void accept(com.hilbertinc.xml.Visitor v);
}
